FROM ubuntu:trusty

ENV VAULT_VERSION 0.4.1

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
      ca-certificates \
      wget \
      unzip \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/* \
    && wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip \
    && unzip vault_${VAULT_VERSION}_linux_amd64.zip \
    && mv vault /usr/local/bin/ \
    && rm -rf vault_${VAULT_VERSION}_linux_amd64.zip

COPY vault.hcl /vault.hcl

EXPOSE 8200

ENTRYPOINT [ "vault" ]
CMD [ "server", "-config", "/vault.hcl" ]
