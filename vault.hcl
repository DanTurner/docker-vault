backend "consul" {
  address = "consul:8500"
  tls_skip_verify = 1
}

listener "tcp" {
 tls_disable = 1
}